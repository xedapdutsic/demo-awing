import React from 'react'
import IconButton from '@mui/material/IconButton'
import IconAdd from '@mui/icons-material/Add'
import { Box } from '@mui/material'

interface IProps {
  handleAddCard: () => void
}

const ButtonAddSubCampaign = ({ handleAddCard }: IProps) => {
  return (
    <Box>
      <IconButton
        aria-label="add"
        size="medium"
        style={{ backgroundColor: ' rgb(237, 237, 237)' }}
        onClick={handleAddCard}
      >
        <IconAdd color="error" />
      </IconButton>
    </Box>
  )
}

export default ButtonAddSubCampaign
