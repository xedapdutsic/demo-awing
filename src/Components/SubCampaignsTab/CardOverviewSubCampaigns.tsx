import React, { memo, useEffect, useState } from 'react'
import { ISubCampaigns } from '../../types/general'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import { Box, Card, CardContent, Tooltip, Typography } from '@mui/material'

interface IProps extends ISubCampaigns {
  index: number
  indexFocus: number
  watchSubCampaigns: {
    id: string
    name: string
    status: boolean
    ads: {
      id: string
      name: string
      quantity: number
    }[]
  }[]
  handleRemoveCard: (id: number) => void
  setIndexFocus: React.Dispatch<React.SetStateAction<number>>
}

const CardOverviewSubCampaigns = ({
  name,
  index,
  status,
  indexFocus,
  ads,
  watchSubCampaigns,
  setIndexFocus,
}: IProps) => {
  const [showError, setShowError] = useState(false)
  useEffect(() => {
    if (
      !watchSubCampaigns[indexFocus].name ||
      watchSubCampaigns[indexFocus].ads.some((e) => !e.name || +e.quantity <= 0)
    ) {
      setShowError(true)
    } else {
      setShowError(false)
    }
    // eslint-disable-next-line
  }, [watchSubCampaigns])

  // console.log(showError)
  return (
    <Box
      onClick={() => setIndexFocus(index)}
      style={{
        overflow: 'inherit',
        margin: '0 10px',
      }}
    >
      <Card
        sx={{ width: 210, height: 120 }}
        className={`${index === indexFocus ? 'active-card' : 'cursor-pointer'} position-relative overflow-inherit `}
      >
        <CardContent className="p-2">
          <Tooltip title={name}>
            <Typography textAlign="center" display={'flex'} justifyContent={'center'} alignItems={'center'}>
              <Typography
                variant="h6"
                textAlign="center"
                color={showError && index === indexFocus ? 'red' : 'inherit'}
                sx={{
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  display: '-webkit-box',
                  WebkitLineClamp: '2',
                  WebkitBoxOrient: 'vertical',
                  wordbreak: 'break-all',
                }}
              >
                {name}
              </Typography>
              <CheckCircleIcon
                htmlColor={status ? '#1b5e20' : 'disabled'}
                style={{
                  paddingLeft: '5px',
                  fontSize: '13px',
                }}
              />
            </Typography>
          </Tooltip>
          <Typography variant="h6" component="div" textAlign="center">
            {+ads.reduce((a, b) => a + +b.quantity, 0)}
          </Typography>
        </CardContent>
      </Card>
    </Box>
  )
}

export default memo(CardOverviewSubCampaigns)
