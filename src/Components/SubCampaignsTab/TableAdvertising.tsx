import * as React from 'react'
import Box from '@mui/material/Box'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import Checkbox from '@mui/material/Checkbox'
import { Button, IconButton, Typography } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import MUITextField from '../MUITextField'
import { Control, UseFormSetValue } from 'react-hook-form'
import DeleteIcon from '@mui/icons-material/Delete'
import { generateUUID } from '../../Utils/general'
import { SubCampaignType } from '../../types/general'

interface Data {
  advName: string
  quantity: string
  addAction: string
}

interface HeadCell {
  id: keyof Data
  label: string | 'custom'
  align: 'left' | 'right'
}

interface IProps {
  control: Control<any, any>
  indexFocus: number
  subCampaigns: SubCampaignType[]
  setValue: UseFormSetValue<{
    subCampaigns: {
      id: string
      name: string
      status: boolean
      // count: number
      ads: any
    }[]
  }>
}

const headCells: readonly HeadCell[] = [
  {
    id: 'advName',
    align: 'left',
    label: 'Tên quảng cáo*',
  },
  {
    id: 'quantity',
    align: 'left',
    label: 'Số lượng*',
  },
  {
    id: 'addAction',
    align: 'right',
    label: 'custom',
  },
]

interface EnhancedTableProps {
  rowCount: number
  numSelected: number
  selected: readonly string[]
  handleAddRow: () => void
  handleRemove: () => void
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { onSelectAllClick, numSelected, rowCount, selected, handleAddRow, handleRemove } = props

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            disabled={!rowCount}
            inputProps={{
              'aria-label': 'select all desserts',
            }}
          />
        </TableCell>

        {!selected.length ? (
          headCells.map((headCell) => (
            <TableCell key={headCell.id} align={headCell.align} padding={'normal'}>
              {headCell.label === 'custom' ? (
                <Button variant="outlined" onClick={handleAddRow}>
                  <AddIcon /> Thêm
                </Button>
              ) : (
                <>{headCell.label}</>
              )}
            </TableCell>
          ))
        ) : (
          <TableCell>
            <IconButton aria-label="delete" onClick={handleRemove}>
              <DeleteIcon />
            </IconButton>
          </TableCell>
        )}
      </TableRow>
    </TableHead>
  )
}

export default function TableAdvertising({ control, indexFocus, subCampaigns, setValue }: IProps) {
  const initData = [{ id: generateUUID(), name: `Quảng cáo ${subCampaigns[indexFocus].ads.length + 1}`, quantity: 1 }]
  const [selected, setSelected] = React.useState<readonly string[]>([])

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelected = subCampaigns[indexFocus].ads.map((n) => n.id)
      setSelected(newSelected)
      return
    }
    setSelected([])
  }

  const handleClick = (event: React.MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id)
    let newSelected: readonly string[] = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1))
    }
    setSelected(newSelected)
  }

  const isSelected = (id: string) => selected.indexOf(id) !== -1

  const handleAddRow = () => {
    setValue(`subCampaigns.${indexFocus}.ads`, [...subCampaigns[indexFocus].ads, ...initData])
  }

  const handleRemoveRow = (index: number) => {
    setValue(`subCampaigns.${indexFocus}.ads`, [
      ...subCampaigns[indexFocus].ads.slice(0, index),
      ...subCampaigns[indexFocus].ads.slice(index + 1),
    ])
  }

  const handleRemoveInHeader = () => {
    if (selected.length === subCampaigns[indexFocus].ads.length) {
      setValue(`subCampaigns.${indexFocus}.ads`, [])
    } else {
      selected.forEach((e, i) => {
        handleRemoveRow(i)
      })
    }
    setSelected([])
  }

  React.useEffect(() => {
    setValue(`subCampaigns.${indexFocus}.ads`, [...subCampaigns[indexFocus].ads])
    // eslint-disable-next-line
  }, [indexFocus])

  return (
    <Box sx={{ width: '100%' }}>
      <Typography variant="h6" textAlign="left" sx={{ py: '16px', mt: '16px', px: 0 }}>
        DANH SÁCH QUẢNG CÁO
      </Typography>

      <Paper sx={{ width: '100%', mb: 2 }}>
        <TableContainer>
          <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle" size={'medium'}>
            <EnhancedTableHead
              selected={selected}
              handleAddRow={handleAddRow}
              numSelected={selected.length}
              rowCount={subCampaigns[indexFocus].ads.length}
              onSelectAllClick={handleSelectAllClick}
              handleRemove={handleRemoveInHeader}
            />
            <TableBody>
              {subCampaigns[indexFocus].ads.map((row, index) => {
                const isItemSelected = isSelected(row.id)
                const labelId = `enhanced-table-checkbox-${index}`

                return (
                  <TableRow
                    hover
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row.id}
                    selected={isItemSelected}
                    sx={{ cursor: 'pointer' }}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        onClick={(event) => handleClick(event, row.id)}
                        color="primary"
                        checked={isItemSelected}
                        inputProps={{
                          'aria-labelledby': labelId,
                        }}
                      />
                    </TableCell>
                    <TableCell align="left">
                      <MUITextField
                        control={control}
                        label=""
                        name={`subCampaigns.${indexFocus}.ads.${index}.name`}
                        required
                      />
                    </TableCell>
                    <TableCell align="left">
                      <MUITextField
                        control={control}
                        label=""
                        name={`subCampaigns.${indexFocus}.ads.${index}.quantity`}
                        type="number"
                        required
                      />
                    </TableCell>
                    <TableCell align="right">
                      <IconButton aria-label="delete" onClick={() => handleRemoveRow(index)}>
                        <DeleteIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Box>
  )
}
