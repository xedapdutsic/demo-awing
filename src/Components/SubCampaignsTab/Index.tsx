import { Box } from '@mui/material'
import React, { useEffect, useState } from 'react'
import ButtonAddSubCampaign from './ButtonAddSubCampaign'
import CardOverviewSubCampaigns from './CardOverviewSubCampaigns'
import { generateUUID } from '../../Utils/general'
import FormUpdateCardOverview from './FormUpdateCardOverview'
import { Control, UseFieldArrayAppend, UseFieldArrayRemove, UseFormSetValue } from 'react-hook-form'
import { SubCampaignType } from '../../types/general'
import TableAdvertising from './TableAdvertising'
import { initDataAds } from '../../App'

interface IProps {
  control: Control<any, any>
  subCampaigns: SubCampaignType[]
  fields: Record<'id', string>[]
  remove: UseFieldArrayRemove
  append: UseFieldArrayAppend<any, string>
  watchSubCampaigns: {
    id: string
    name: string
    status: boolean
    ads: {
      id: string
      name: string
      quantity: number
    }[]
  }[]
  setValue: UseFormSetValue<{
    subCampaigns: {
      id: string
      name: string
      status: boolean
      ads: any
    }[]
  }>
}

const SubCampaignsTab = ({ control, subCampaigns, append, fields, remove, setValue, watchSubCampaigns }: IProps) => {
  const [indexFocus, setIndexFocus] = useState(0)
  // const []

  const handleAddCard = () => {
    append([
      {
        count: 0,
        name: `Chiến dịch con ${fields.length + 1}`,
        id: generateUUID(),
        status: true,
        ads: initDataAds,
      },
    ])
  }

  const handleRemoveCard = (index: number) => {
    remove(index)
  }

  useEffect(() => {
    setValue(`subCampaigns.${indexFocus}.status`, subCampaigns[indexFocus].status)
    setValue(`subCampaigns.${indexFocus}.name`, subCampaigns[indexFocus].name)
    // eslint-disable-next-line
  }, [indexFocus])

  return (
    <Box>
      <Box component={'div'} overflow={'auto'}>
        <Box component={'div'} display="flex" flexDirection="row">
          <ButtonAddSubCampaign handleAddCard={handleAddCard} />
          <>
            {subCampaigns.map((e, i: number) => (
              <CardOverviewSubCampaigns
                {...e}
                key={i}
                index={i}
                watchSubCampaigns={watchSubCampaigns}
                indexFocus={indexFocus}
                setIndexFocus={setIndexFocus}
                handleRemoveCard={handleRemoveCard}
              />
            ))}
          </>
        </Box>
      </Box>
      <>
        <FormUpdateCardOverview control={control} index={indexFocus} />
      </>
      <TableAdvertising control={control} indexFocus={indexFocus} subCampaigns={subCampaigns} setValue={setValue} />
    </Box>
  )
}

export default SubCampaignsTab
