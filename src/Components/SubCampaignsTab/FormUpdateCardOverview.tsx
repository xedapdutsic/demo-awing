import { Grid } from '@mui/material'
import React from 'react'
import MUITextField from '../MUITextField'
import { Control } from 'react-hook-form'
import { FormInputMultiCheckbox } from '../FormInputMultiCheckbox'

interface IProps {
  index: number
  control: Control<any, any>
}

const FormUpdateCardOverview = ({ control, index }: IProps) => {
  return (
    <Grid container spacing={2}>
      <Grid item xs={8} my="1em">
        <MUITextField
          name={`subCampaigns.${index}.name`}
          label="Tên chiến dịch con"
          control={control}
          required={true}
        />
      </Grid>
      <Grid item xs={4}>
        <FormInputMultiCheckbox control={control} label="Đang hoạt động" name={`subCampaigns.${index}.status`} />
      </Grid>
    </Grid>
  )
}

export default FormUpdateCardOverview
