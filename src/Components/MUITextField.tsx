import { TextField } from '@mui/material'
import React from 'react'
import { Controller } from 'react-hook-form'
import { FormInputProps } from '../types/general'

const MUITextField = ({ name, control, setValue, label, required = false, type = 'text' }: FormInputProps) => {
  return (
    <Controller
      name={name}
      rules={{
        required,
      }}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error }, formState }) => {
        return (
          <TextField
            type={type}
            required={required}
            helperText={error ? 'Dữ liệu không hợp lệ' : null}
            size="small"
            error={!!error}
            onChange={(e) => {
              setValue && setValue(e.target.value)
              onChange(e)
            }}
            value={value}
            fullWidth
            label={label}
            variant="standard"
          />
        )
      }}
    />
  )
}

export default MUITextField
