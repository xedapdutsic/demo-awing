import React from 'react'
import { Checkbox, FormControl, FormControlLabel } from '@mui/material'
import { Controller } from 'react-hook-form'
import { FormInputProps } from '../types/general'

export const FormInputMultiCheckbox: React.FC<FormInputProps> = ({ name, control, label, setValue }) => {
  return (
    <FormControl size={'small'} variant={'outlined'}>
      <FormControlLabel
        control={
          <Controller
            name={name}
            render={({ field: props }) => (
              <Checkbox {...props} checked={props.value} onChange={(e) => props.onChange(e.target.checked)} />
            )}
            control={control}
          />
        }
        label={label}
      />
    </FormControl>
  )
}
