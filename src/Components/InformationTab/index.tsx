import { Grid } from '@mui/material'
import React from 'react'
import MUITextField from '../MUITextField'
import { Control } from 'react-hook-form'

interface IProps {
  control: Control<any, any>
}

const InformationTab = ({ control }: IProps) => {
  return (
    <>
      <Grid item xs={12} my="3">
        <MUITextField name="name" label="Tên chiến dịch" control={control} required={true} />
      </Grid>
      <Grid item xs={12} marginTop="15px">
        <MUITextField name="describe" label="Mô tả" control={control} />
      </Grid>
    </>
  )
}

export default InformationTab
