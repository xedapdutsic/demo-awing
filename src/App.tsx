import React, { useEffect, useState } from 'react'
import './App.css'
import { AppBar, Box, Container, Paper, Tab, Tabs, Toolbar, Typography, Button, Stack } from '@mui/material'
import InformationTab from './Components/InformationTab'
import { useFieldArray, useForm, useWatch } from 'react-hook-form'
import SubCampaignsTab from './Components/SubCampaignsTab/Index'
import { generateUUID } from './Utils/general'
import CustomTabPanel from './Components/CustomTabPanel'
import { IInformation } from './types/general'

export const initDataAds = [{ id: generateUUID(), name: 'Quảng cáo 1', quantity: 1 }]
function App() {
  const [valueTabs, setValueTabs] = useState(0)
  const [disableSubmit, setDisableSubmit] = useState(false)
  const { handleSubmit, formState, control, watch, setValue, setError, clearErrors, getValues } = useForm({
    defaultValues: {
      subCampaigns: [{ id: generateUUID(), name: 'Chiến dịch con 1', status: true, ads: initDataAds }],
    },
  })

  // biến dùng để validate
  const watchSubCampaigns = useWatch({
    control,
    name: 'subCampaigns',
  })

  const { subCampaigns } = watch()

  const { fields, append, remove } = useFieldArray<any>({
    control,
    name: 'subCampaigns',
  })

  // Accessing status fields
  const { errors, isSubmitting } = formState

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValueTabs(newValue)
  }

  const onSubmit = async (values: any) => {
    if (!disableSubmit) {
      const campaign = {
        information: {
          describe: values.describe,
          name: values.name,
        },
        subCampaigns: values.subCampaigns,
      }
      const alertValue = `Thêm thành công chiến dịch ${JSON.stringify({ campaign })}`
      campaign.information?.name && window.alert(alertValue)
    }
  }

  const handleValidate = () => {
    watchSubCampaigns.forEach((e, index) => {
      if (!e.name) {
        setError(`subCampaigns.${index}.name`, { type: 'custom', message: 'Dữ liệu không hợp lệ' })
      }
      e.ads.forEach((el, i) => {
        if (+el.quantity <= 0) {
          setError(`subCampaigns.${index}.ads.${i}.quantity`, { type: 'custom', message: '' })
          setDisableSubmit(true)
        } else {
          clearErrors(`subCampaigns.${index}.ads.${i}.quantity`)
          setDisableSubmit(false)
        }
      })
    })
  }

  // lắng nghe thay đổi để validate form
  useEffect(() => {
    handleValidate()
    // eslint-disable-next-line
  }, [watchSubCampaigns, isSubmitting])

  useEffect(() => {
    const information: IInformation | undefined = getValues() as any

    if (!information?.name && isSubmitting) {
      setDisableSubmit(true)
      window.alert('Vui lòng điền đúng và đầy đủ thông tin')
    } else if (isSubmitting && Object.keys(errors).length !== 0) {
      setDisableSubmit(true)
      window.alert('Vui lòng điền đúng và đầy đủ thông tin')
    } else {
      setDisableSubmit(false)
    }
    // eslint-disable-next-line
  }, [isSubmitting])

  return (
    <>
      <AppBar
        position="absolute"
        color="default"
        elevation={0}
        sx={{
          position: 'relative',
          borderBottom: (t) => `1px solid ${t.palette.divider}`,
        }}
      >
        <Toolbar>
          <Stack spacing={{ xs: 1, sm: 2 }} direction="row" justifyContent={'space-between'} width="100%">
            <Typography variant="h6" color="inherit" noWrap>
              DEMO-AWING
            </Typography>
            <Button onClick={handleSubmit(onSubmit)} variant="contained" color="primary">
              Submit
            </Button>
          </Stack>
        </Toolbar>
      </AppBar>
      <Container component="main" maxWidth="xl" sx={{ mb: 4 }}>
        <Paper variant="outlined" sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={valueTabs} onChange={handleChange} aria-label="basic tabs">
              <Tab label="Thông tin" />
              <Tab label="Chiến dịch con" />
            </Tabs>
          </Box>

          <CustomTabPanel value={valueTabs} index={0}>
            <InformationTab control={control} />
          </CustomTabPanel>
          <CustomTabPanel value={valueTabs} index={1}>
            <SubCampaignsTab
              control={control}
              fields={fields}
              append={append}
              remove={remove}
              subCampaigns={subCampaigns}
              setValue={setValue}
              watchSubCampaigns={watchSubCampaigns}
            />
          </CustomTabPanel>
        </Paper>
      </Container>
    </>
  )
}

export default App
