import React from 'react'
import { Control } from 'react-hook-form'

export interface FormInputProps {
  name: string
  control: Control<any, any>
  label: string
  setValue?: any
  required?: boolean
  type?: React.HTMLInputTypeAttribute
}

export interface IInformation {
  name: string
  describe?: string
}

export type ISubCampaigns = {
  name: string
  status: boolean
  ads: IAds[]
  count?: number
}

export interface IAds {
  name: string
  quantity: number
  id: string
}

export interface ICampaign {
  information: IInformation
  subCampaigns: ISubCampaigns[]
}

export interface SubCampaignType extends ISubCampaigns {
  id: string
  // control: Control<FieldValues, any>
  // name: string
}
